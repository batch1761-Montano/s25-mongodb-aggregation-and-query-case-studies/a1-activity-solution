db.fruits.insertMany([

	{

		"name": "Apple",

		"supplier": "Red Farms Inc.",

                "stocks": 20,

		"price": 40,

		"onSale": true

	},

        {

		"name": "Banana",

		"supplier": "Yellow Farms Inc.",

                "stocks": 15,

		"price": 20,

		"onSale": true

	},

        {

		"name": "Kiwi",

		"supplier": "Green Farms Inc.",

                "stocks": 25,

		"price": 50,

		"onSale": true

	},

        {

		"name": "Mango",

		"supplier": "Yellow Farms Inc.",

                "stocks": 10,

		"price": 60,

		"onSale": true

	},

        {

		"name": "Dragon Fruit",

		"supplier": "Red Farms Inc.",

                "stocks": 10,

		"price": 60,

		"onSale": true

	}

])  

//Activity: (AGGREGATE-176)  
  
    // Aggregate to count the total number of items supplied by Red Farms Inc. ($count stage)
    
    db.fruits.aggregate([
    
           {$match:{supplier:{$regex:'Red Farms Inc.',$options:'$i'}}},
           {$count:"itemsRedFarms"}
    ])
    // Aggregate to count the total number of items with price greater than 50($count stage)
    
    db.fruits.aggregate([
    
           {$match:{price:{$gt:50}}},
           {$count:"priceGreaterThan50"}
    ])       
    
    // Aggregate to get the average price of all fruits that are onSale per supplier($group)
    
    db.fruits.aggregate([
    
        {$match: {onSale:true}},
        {$group:{_id:"$supplier",avgPricePerSupplier:{$avg:"$price"}}}
    
    ])
    // Aggragate to get the highest price of fruits that are onSale per supplier. ($group)
    
    db.fruits.aggregate([
    
        {$match: {onSale:true}},
        {$group:{_id:"$supplier",maxPricePerSupplier:{$max:"$price"}}}
    
    ])
    
    // Aggregate to get the lowest price of fruits that are onSale per supplier. ($group)
    
    db.fruits.aggregate([
    
    {$match: {onSale:true}},
    {$group:{_id:"$supplier",minPricePerSupplier:{$min:"$price"}}}
    
    ])
    
    